import expensesReducers from '../../reducers/expenses';
import expenses from '../fixtures/expenses';

test('should state default state', () => {
   const state = expensesReducers(undefined, {type: '@@INIT'});
   expect(state).toEqual([]);
});

test('should remove expense by id', () => {
   const action = {
        type: 'REMOVE_EXPENSE',
        id: expenses[1].id
   };

   const state = expensesReducers(expenses, action);
   expect(state.length).toBe(2);
   expect(state).toEqual([expenses[0], expenses[2]]);
});

test('should not remove expense id if if not found', () => {
   const action = {
       type: 'REMOVE_EXPENSE',
       id: '-id'
   };
   const  state =  expensesReducers(expenses, action);
   expect(state).toEqual(expenses);
});

test('should add an expense', () => {
   const expense = {
       id: '4',
       description: 'Gym',
       note: 'asdf',
       amount: 1959,
       createdAt: 0
   };
   const action = {type: 'ADD_EXPENSE', expense};
   const state = expensesReducers(expenses, action);
   expect(state.length).toBe(4);
   expect(state).toEqual([...expenses, expense]);
});

test('should edit expense', () => {
   const updates = {
       description: 'Car Rent',
       note: 'rols roice',
       amount: 1000000,
       createdAt: 0
   };
   const action = {type: 'EDIT_EXPENSE',id: '1', updates};
  const state = expensesReducers(expenses, action);
  expect(state[0]).toEqual({...updates, id: '1'})
});

test('should not edit with invalid id', () => {
    const updates = {
        description: 'Car Rent',
        note: 'rols roice',
        amount: 1000000,
        createdAt: 0
    };
    const action = {type: 'EDIT_EXPENSE', id: '-1', updates};
    const state = expensesReducers(expenses, action);
    expect(state).toEqual(expenses)
});